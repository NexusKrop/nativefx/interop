//------------------------------------------------------------------------------
// <auto-generated>
//      Generated by NativeAssist version 1.0.0.0 with Split Generator.
//      Do not modify this file unless necessary.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NativeFx.Interop;

using System;
using GTA.Native;
using GTA;
using GTA.Math;

public static partial class Natives 
{
public static class Interior {
/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1493<br />
/// <b>Native ID:</b> 0xF49B58631D9E22D9<br />
/// </para>
/// </remarks>
public static float GetInteriorHeading(int /* Interior */ interior){
return Function.Call<float>((Hash)0xF49B58631D9E22D9uL, interior);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1290<br />
/// <b>Native ID:</b> 0x252BDC06B73FA6EA<br />
/// </para>
/// </remarks>
public static void GetInteriorLocationAndNamehash(int /* Interior */ interior, ref Vector3 position, ref uint nameHash){
var nativeAssistPointerVar0 = position;
var nativeAssistPointerVar1 = nameHash;
unsafe {
Function.Call((Hash)0x252BDC06B73FA6EAuL, interior, &nativeAssistPointerVar0, &nativeAssistPointerVar1);
}
position = nativeAssistPointerVar0;
nameHash = nativeAssistPointerVar1;
}

/// <summary>
/// Returns the group ID of the specified interior. For example, regular interiors have group 0, subway interiors have group 1. There are a few other groups too.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xE4A84ABF135EF91A<br />
/// </para>
/// </remarks>
public static int GetInteriorGroupId(int /* Interior */ interior){
return Function.Call<int>((Hash)0xE4A84ABF135EF91AuL, interior);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x9E3B3E6D66F6E22F<br />
/// </para>
/// </remarks>
public static Vector3 GetOffsetFromInteriorInWorldCoords(int /* Interior */ interior, float x, float y, float z){
return Function.Call<Vector3>((Hash)0x9E3B3E6D66F6E22FuL, interior, x, y, z);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xBC72B5D7A1CBD54D<br />
/// </para>
/// </remarks>
public static bool IsInteriorScene(){
return Function.Call<bool>((Hash)0xBC72B5D7A1CBD54DuL);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x26B0E73D7EAAF4D3<br />
/// </para>
/// </remarks>
public static bool IsValidInterior(int /* Interior */ interior){
return Function.Call<bool>((Hash)0x26B0E73D7EAAF4D3uL, interior);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xB365FC0C4E27FFA7<br />
/// </para>
/// </remarks>
public static void ClearRoomForEntity(int /* Entity */ entity){
Function.Call((Hash)0xB365FC0C4E27FFA7uL, entity);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x52923C4710DD9907<br />
/// </para>
/// </remarks>
public static void ForceRoomForEntity(int /* Entity */ entity, int /* Interior */ interior, uint roomHashKey){
Function.Call((Hash)0x52923C4710DD9907uL, entity, interior, roomHashKey);
}

/// <summary>
/// Gets the room hash key from the room that the specified entity is in. Each room in every interior has a unique key. Returns 0 if the entity is outside.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x47C2A06D4F5F424B<br />
/// </para>
/// </remarks>
public static uint GetRoomKeyFromEntity(int /* Entity */ entity){
return Function.Call<uint>((Hash)0x47C2A06D4F5F424BuL, entity);
}

/// <summary>
/// Seems to do the exact same as INTERIOR::GET_ROOM_KEY_FROM_ENTITY
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x399685DB942336BC<br />
/// </para>
/// </remarks>
public static uint GetKeyForEntityInRoom(int /* Entity */ entity){
return Function.Call<uint>((Hash)0x399685DB942336BCuL, entity);
}

/// <summary>
/// Returns the handle of the interior that the entity is in. Returns 0 if outside.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x2107BA504071A6BB<br />
/// </para>
/// </remarks>
public static int GetInteriorFromEntity(int /* Entity */ entity){
return Function.Call<int>((Hash)0x2107BA504071A6BBuL, entity);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x82EBB79E258FA2B7<br />
/// </para>
/// </remarks>
public static void RetainEntityInInterior(int /* Entity */ entity, int /* Interior */ interior){
Function.Call((Hash)0x82EBB79E258FA2B7uL, entity, interior);
}

/// <summary>
/// Immediately removes entity from an interior. Like sets entity to `limbo` room.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 2189<br />
/// <b>Native ID:</b> 0x85D5422B2039A70D<br />
/// </para>
/// </remarks>
public static void ClearInteriorStateOfEntity(int /* Entity */ entity){
Function.Call((Hash)0x85D5422B2039A70DuL, entity);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1493<br />
/// <b>Native ID:</b> 0x38C1CB1CB119A016<br />There were parameter(s) that was labeled as <b>Any</b>, and these had been written as <c>int</c> by generator.
/// </para>
/// </remarks>
public static void ForceActivatingTrackingOnEntity(int /* bug: Any */ p0, int /* bug: Any */ p1){
Function.Call((Hash)0x38C1CB1CB119A016uL, p0, p1);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x920D853F3E17F1DA<br />
/// </para>
/// </remarks>
public static void ForceRoomForGameViewport(int interiorID, uint roomHashKey){
Function.Call((Hash)0x920D853F3E17F1DAuL, interiorID, roomHashKey);
}

/// <summary>
/// Example of use (carmod_shop)<br />INTERIOR::SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME(&quot;V_CarModRoom&quot;);
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xAF348AFCB575A441<br />
/// </para>
/// </remarks>
public static void SetRoomForGameViewportByName(string roomName){
Function.Call((Hash)0xAF348AFCB575A441uL, roomName);
}

/// <summary>
/// Usage: INTERIOR::SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(INTERIOR::GET_KEY_FOR_ENTITY_IN_ROOM(PLAYER::PLAYER_PED_ID()));
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x405DC2AEF6AF95B9<br />
/// </para>
/// </remarks>
public static void SetRoomForGameViewportByKey(uint roomHashKey){
Function.Call((Hash)0x405DC2AEF6AF95B9uL, roomHashKey);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xA6575914D2A0B450<br />
/// </para>
/// </remarks>
public static uint GetRoomKeyForGameViewport(){
return Function.Call<uint>((Hash)0xA6575914D2A0B450uL);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x23B59D8912F94246<br />
/// </para>
/// </remarks>
public static void ClearRoomForGameViewport(){
Function.Call((Hash)0x23B59D8912F94246uL);
}

/// <summary>
/// Returns the current interior id from gameplay camera
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1604<br />
/// <b>Native ID:</b> 0xE7D267EC6CA966C3<br />
/// </para>
/// </remarks>
public static int GetInteriorFromPrimaryView(){
return Function.Call<int>((Hash)0xE7D267EC6CA966C3uL);
}

/// <summary>
/// Returns interior ID from specified coordinates. If coordinates are outside, then it returns 0.<br /><br />Example for VB.NET<br />Dim interiorID As Integer = Native.Function.Call(Of Integer)(Hash.GET_INTERIOR_AT_COORDS, X, Y, Z)
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xB0F7F8663821D9C3<br />
/// </para>
/// </remarks>
public static int GetInteriorAtCoords(float x, float y, float z){
return Function.Call<int>((Hash)0xB0F7F8663821D9C3uL, x, y, z);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x3F6167F351168730<br />
/// </para>
/// </remarks>
public static void AddPickupToInteriorRoomByName(Pickup pickup, string roomName){
Function.Call((Hash)0x3F6167F351168730uL, pickup, roomName);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x2CA429C029CCF247<br />
/// </para>
/// </remarks>
public static void PinInteriorInMemory(int /* Interior */ interior){
Function.Call((Hash)0x2CA429C029CCF247uL, interior);
}

/// <summary>
/// Does something similar to INTERIOR::DISABLE_INTERIOR.<br /><br />You don&apos;t fall through the floor but everything is invisible inside and looks the same as when INTERIOR::DISABLE_INTERIOR is used. Peds behaves normally inside. 
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x261CCE7EED010641<br />
/// </para>
/// </remarks>
public static void UnpinInterior(int /* Interior */ interior){
Function.Call((Hash)0x261CCE7EED010641uL, interior);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x6726BDCCC1932F0E<br />
/// </para>
/// </remarks>
public static bool IsInteriorReady(int /* Interior */ interior){
return Function.Call<bool>((Hash)0x6726BDCCC1932F0EuL, interior);
}

/// <summary>
/// Only used once in the entire game scripts.<br />Does not actually return anything.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x4C2330E61D3DEB56<br />
/// </para>
/// </remarks>
public static bool SetInteriorInUse(int /* Interior */ interior){
return Function.Call<bool>((Hash)0x4C2330E61D3DEB56uL, interior);
}

/// <summary>
/// Returns the interior ID representing the requested interior at that location (if found?). The supplied interior string is not the same as the one used to load the interior.<br /><br />Use: INTERIOR::UNPIN_INTERIOR(INTERIOR::GET_INTERIOR_AT_COORDS_WITH_TYPE(x, y, z, interior))<br /><br />Interior types include: &quot;V_Michael&quot;, &quot;V_Franklins&quot;, &quot;V_Franklinshouse&quot;, etc.. you can find them in the scripts.<br /><br />Not a very useful native as you could just use GET_INTERIOR_AT_COORDS instead and get the same result, without even having to specify the interior type.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x05B7A89BD78797FC<br />
/// </para>
/// </remarks>
public static int GetInteriorAtCoordsWithType(float x, float y, float z, string interiorType){
return Function.Call<int>((Hash)0x05B7A89BD78797FCuL, x, y, z, interiorType);
}

/// <summary>
/// Hashed version of GET_INTERIOR_AT_COORDS_WITH_TYPE
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xF0F77ADB9F67E79D<br />
/// </para>
/// </remarks>
public static int GetInteriorAtCoordsWithTypehash(float x, float y, float z, uint typeHash){
return Function.Call<int>((Hash)0xF0F77ADB9F67E79DuL, x, y, z, typeHash);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1103<br />
/// <b>Native ID:</b> 0x483ACA1176CA93F1<br />
/// </para>
/// </remarks>
public static void ActivateInteriorGroupsUsingCamera(){
Function.Call((Hash)0x483ACA1176CA93F1uL);
}

/// <summary>
/// Returns true if the collision at the specified coords is marked as being outside (false if there&apos;s an interior)
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xEEA5AC2EDA7C33E8<br />
/// </para>
/// </remarks>
public static bool IsCollisionMarkedOutside(float x, float y, float z){
return Function.Call<bool>((Hash)0xEEA5AC2EDA7C33E8uL, x, y, z);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xEC4CF9FCB29A4424<br />
/// </para>
/// </remarks>
public static int GetInteriorFromCollision(float x, float y, float z){
return Function.Call<int>((Hash)0xEC4CF9FCB29A4424uL, x, y, z);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 1604<br />
/// <b>Native ID:</b> 0x7ECDF98587E92DEC<br />
/// </para>
/// </remarks>
public static void EnableStadiumProbesThisFrame(bool toggle){
Function.Call((Hash)0x7ECDF98587E92DECuL, toggle);
}

/// <summary>
/// More info: http://gtaforums.com/topic/836367-adding-props-to-interiors/<br /><br />Full list of IPLs and interior entity sets by DurtyFree: https://github.com/DurtyFree/gta-v-data-dumps/blob/master/ipls.json
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x55E86AF2712B36A1<br />
/// </para>
/// </remarks>
public static void ActivateInteriorEntitySet(int /* Interior */ interior, string entitySetName){
Function.Call((Hash)0x55E86AF2712B36A1uL, interior, entitySetName);
}

/// <summary>
/// Full list of IPLs and interior entity sets by DurtyFree: https://github.com/DurtyFree/gta-v-data-dumps/blob/master/ipls.json
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x420BD37289EEE162<br />
/// </para>
/// </remarks>
public static void DeactivateInteriorEntitySet(int /* Interior */ interior, string entitySetName){
Function.Call((Hash)0x420BD37289EEE162uL, interior, entitySetName);
}

/// <summary>
/// Full list of IPLs and interior entity sets by DurtyFree: https://github.com/DurtyFree/gta-v-data-dumps/blob/master/ipls.json
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x35F7DD45E8C0A16D<br />
/// </para>
/// </remarks>
public static bool IsInteriorEntitySetActive(int /* Interior */ interior, string entitySetName){
return Function.Call<bool>((Hash)0x35F7DD45E8C0A16DuL, interior, entitySetName);
}

/// <summary>
/// Full list of IPLs and interior entity sets by DurtyFree: https://github.com/DurtyFree/gta-v-data-dumps/blob/master/ipls.json
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 877<br />
/// <b>Native ID:</b> 0xC1F1920BAF281317<br />
/// </para>
/// </remarks>
public static void SetInteriorEntitySetTintIndex(int /* Interior */ interior, string entitySetName, int color){
Function.Call((Hash)0xC1F1920BAF281317uL, interior, entitySetName, color);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x41F37C3427C75AE0<br />
/// </para>
/// </remarks>
public static void RefreshInterior(int /* Interior */ interior){
Function.Call((Hash)0x41F37C3427C75AE0uL, interior);
}

/// <summary>
/// This is the native that is used to hide the exterior of GTA Online apartment buildings when you are inside an apartment.<br /><br />More info: http://gtaforums.com/topic/836301-hiding-gta-online-apartment-exteriors/
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xA97F257D0151A6AB<br />
/// </para>
/// </remarks>
public static void EnableExteriorCullModelThisFrame(uint mapObjectHash){
Function.Call((Hash)0xA97F257D0151A6ABuL, mapObjectHash);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 757<br />
/// <b>Native ID:</b> 0x50C375537449F369<br />
/// </para>
/// </remarks>
public static void EnableShadowCullModelThisFrame(uint mapObjectHash){
Function.Call((Hash)0x50C375537449F369uL, mapObjectHash);
}

/// <summary>
/// Example: <br />This removes the interior from the strip club and when trying to walk inside the player just falls:<br /><br />INTERIOR::DISABLE_INTERIOR(118018, true);
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x6170941419D7D8EC<br />
/// </para>
/// </remarks>
public static void DisableInterior(int /* Interior */ interior, bool toggle){
Function.Call((Hash)0x6170941419D7D8ECuL, interior, toggle);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xBC5115A5A939DD15<br />
/// </para>
/// </remarks>
public static bool IsInteriorDisabled(int /* Interior */ interior){
return Function.Call<bool>((Hash)0xBC5115A5A939DD15uL, interior);
}

/// <summary>
/// Does something similar to INTERIOR::DISABLE_INTERIOR
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xD9175F941610DB54<br />
/// </para>
/// </remarks>
public static void CapInterior(int /* Interior */ interior, bool toggle){
Function.Call((Hash)0xD9175F941610DB54uL, interior, toggle);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x92BAC8ACF88CEC26<br />
/// </para>
/// </remarks>
public static bool IsInteriorCapped(int /* Interior */ interior){
return Function.Call<bool>((Hash)0x92BAC8ACF88CEC26uL, interior);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x9E6542F0CE8E70A3<br />
/// </para>
/// </remarks>
public static void DisableMetroSystem(bool toggle){
Function.Call((Hash)0x9E6542F0CE8E70A3uL, toggle);
}

/// <summary>
/// Jenkins hash _might_ be 0xFC227584.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Interior<br />
/// <b>From Build:</b> 791<br />
/// <b>Native ID:</b> 0x7241CCB7D020DB69<br />
/// </para>
/// </remarks>
public static void SetIsExteriorOnly(int /* Entity */ entity, bool toggle){
Function.Call((Hash)0x7241CCB7D020DB69uL, entity, toggle);
}

}}
