//------------------------------------------------------------------------------
// <auto-generated>
//      Generated by NativeAssist version 1.0.0.0 with Split Generator.
//      Do not modify this file unless necessary.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NativeFx.Interop;

using System;
using GTA.Native;
using GTA;
using GTA.Math;

public static partial class Natives 
{
public static class Decorator {
/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x95AED7B8E39ECAA4<br />
/// </para>
/// </remarks>
public static bool DecorSetTime(int /* Entity */ entity, string propertyName, int timestamp){
return Function.Call<bool>((Hash)0x95AED7B8E39ECAA4uL, entity, propertyName, timestamp);
}

/// <summary>
/// This function sets metadata of type bool to specified entity.<br />
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x6B1E8E2ED1335B71<br />
/// </para>
/// </remarks>
public static bool DecorSetBool(int /* Entity */ entity, string propertyName, bool value){
return Function.Call<bool>((Hash)0x6B1E8E2ED1335B71uL, entity, propertyName, value);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x211AB1DD8D0F363A<br />
/// </para>
/// </remarks>
public static bool DecorSetFloat(int /* Entity */ entity, string propertyName, float value){
return Function.Call<bool>((Hash)0x211AB1DD8D0F363AuL, entity, propertyName, value);
}

/// <summary>
/// Sets property to int.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x0CE3AA5E1CA19E10<br />
/// </para>
/// </remarks>
public static bool DecorSetInt(int /* Entity */ entity, string propertyName, int value){
return Function.Call<bool>((Hash)0x0CE3AA5E1CA19E10uL, entity, propertyName, value);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xDACE671663F2F5DB<br />
/// </para>
/// </remarks>
public static bool DecorGetBool(int /* Entity */ entity, string propertyName){
return Function.Call<bool>((Hash)0xDACE671663F2F5DBuL, entity, propertyName);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x6524A2F114706F43<br />
/// </para>
/// </remarks>
public static float DecorGetFloat(int /* Entity */ entity, string propertyName){
return Function.Call<float>((Hash)0x6524A2F114706F43uL, entity, propertyName);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xA06C969B02A97298<br />
/// </para>
/// </remarks>
public static int DecorGetInt(int /* Entity */ entity, string propertyName){
return Function.Call<int>((Hash)0xA06C969B02A97298uL, entity, propertyName);
}

/// <summary>
/// Returns whether or not the specified property is set for the entity.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x05661B80A8C9165F<br />
/// </para>
/// </remarks>
public static bool DecorExistOn(int /* Entity */ entity, string propertyName){
return Function.Call<bool>((Hash)0x05661B80A8C9165FuL, entity, propertyName);
}

/// <summary>
/// <i>No description available.</i>
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x00EE9F297C738720<br />
/// </para>
/// </remarks>
public static bool DecorRemove(int /* Entity */ entity, string propertyName){
return Function.Call<bool>((Hash)0x00EE9F297C738720uL, entity, propertyName);
}

/// <summary>
/// https://alloc8or.re/gta5/doc/enums/eDecorType.txt
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x9FD90732F56403CE<br />
/// </para>
/// </remarks>
public static void DecorRegister(string propertyName, int type){
Function.Call((Hash)0x9FD90732F56403CEuL, propertyName, type);
}

/// <summary>
/// type: see DECOR_REGISTER
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0x4F14F9F870D6FBC8<br />
/// </para>
/// </remarks>
public static bool DecorIsRegisteredAsType(string propertyName, int type){
return Function.Call<bool>((Hash)0x4F14F9F870D6FBC8uL, propertyName, type);
}

/// <summary>
/// Called after all decorator type initializations.
/// </summary>
/// <remarks>
/// <para>
/// <b>In Namespace:</b> Decorator<br />
/// <b>From Build:</b> 323<br />
/// <b>Native ID:</b> 0xA9D14EEA259F9248<br />
/// </para>
/// </remarks>
public static void DecorRegisterLock(){
Function.Call((Hash)0xA9D14EEA259F9248uL);
}

}}
