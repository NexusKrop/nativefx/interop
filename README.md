# NativeFx.Interop

This library provides a `Natives.cs` native wrapper file generated from `alloc8or`'s `natives.json` via
[the Native Interop Generator](https://gitlab.com/NexusKrop/nativefx/interop-generator) tool
for use with Community Script Hook V .NET API version 3.

## Install

### For developers

Use NuGet gallery. Make sure you update to the latest version, as it may contain critical fixes.

### For users

Download the latest `.zip` file from the releases section and put it into your `scripts` folder. Please note that you have to update this library as least every time the game updates (after SHV released).

Do not overwrite the `NativeFx.Interop` files unless the one provided to overwrite such file is newer than the same file you already have. Overwritting with an older version will downgrade your `NativeFx.Interop` version and cause issues with scripts.

## Updates

Updates are only worked on for each new game build if SHV is released. Due to current international situation, SHV updates may face an inevitable delay.

### Compatiblity

For most part all updates should be source and binary compatible. Breaking changes may occour if `NativeAssist` have had a breaking change, which may occur
at the current stage.

Watch out for SemVer increase for each update to determine compatiblity.

## Licence

As it is an automatically generated file, this project is licensed under the
MIT License (otherwise known as Expat licence).